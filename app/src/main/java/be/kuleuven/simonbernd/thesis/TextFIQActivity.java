package be.kuleuven.simonbernd.thesis;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.NumberPicker;

import be.kuleuven.simonbernd.thesis.model.AnswerWrapper;
import be.kuleuven.simonbernd.thesis.model.Question;

/**
 * Created by Bernd on 18/03/2018.
 */

public class TextFIQActivity extends FIQuestionActivity {
    private EditText widget;
    @Override
    protected void setWidget(AnswerWrapper answerWrapper) {
        if(widget == null){
            createWidget();
        }
        widget.setText(answerWrapper.text);
    }

    @Override
    protected void createWidget() {
        widget = new EditText(this);
        ViewGroup layout = findViewById(R.id.root_layout);
        layout.addView(widget);

    }
}
