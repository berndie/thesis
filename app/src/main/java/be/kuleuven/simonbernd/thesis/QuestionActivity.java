package be.kuleuven.simonbernd.thesis;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.firebase.jobdispatcher.Constraint;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.Lifetime;
import com.firebase.jobdispatcher.Trigger;

import java.util.List;

import be.kuleuven.simonbernd.thesis.model.AnswerWrapper;
import be.kuleuven.simonbernd.thesis.model.Question;
import be.kuleuven.simonbernd.thesis.service.SendAnswersJobService;

public abstract class QuestionActivity extends AppCompatActivity {
    public static final String QUESTION_NUMBER_KEY = "question_number";
    protected int questionNumber;
    protected List<AnswerWrapper> answers;
    protected QuestionViewModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       if(this.getSharedPreferences(getString(R.string.shared_pref_file), Context.MODE_PRIVATE)
                .getBoolean(getString(R.string.pref_filled_in), false)){
           startActivity(new Intent(this, LogoutActivity.class));
       }
        getQuestionNumber();
        setContentView(R.layout.activity_question);
        ((Button) findViewById(R.id.next_button)).setOnClickListener(forwards());
        ((Button) findViewById(R.id.previous_button)).setOnClickListener(backwards());
        model = ViewModelProviders.of(this).get(QuestionViewModel.class);
        model.getQuestion(this, questionNumber).observe(this, new Observer<Question>(){
            @Override
            public void onChanged(@Nullable Question question) {
                setQuestion(question);
            }
        });
        model.getAnswers(this, questionNumber).observe(this, new Observer<List<AnswerWrapper>>(){
            @Override
            public void onChanged(@Nullable List<AnswerWrapper> answers) {
                setAnswersWrapper(answers);
            }
        });





    }

    protected void setQuestion(Question question){
        ((TextView)findViewById(R.id.question)).setText(question.text);
    }

    private void getQuestionNumber(){
        try {
            questionNumber = getIntent().getExtras().getInt(QUESTION_NUMBER_KEY);
        }
        catch (NullPointerException e){
            questionNumber = 1;
        }
    }

    private void setAnswersWrapper(List<AnswerWrapper> answers){
        this.answers = answers;
        setAnswers(answers);
    }

    protected abstract void setAnswers(List<AnswerWrapper> answers);

    protected View.OnClickListener forwards(){
        return  new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goTo(questionNumber + 1);
            }
        };
    }
    protected View.OnClickListener backwards(){
        return  new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goTo(questionNumber - 1);
            }
        };
    }

    protected void goTo(int questionNumber){
        model.getQuestion(this, questionNumber).observe(this, new Observer<Question>() {
            @Override
            public void onChanged(@Nullable Question question) {
                goToHelper(question);
            }
        });
    }

    private void goToHelper(Question question){
        Intent intent;
        if(question == null){
            FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));
            Job job = dispatcher.newJobBuilder().setService(SendAnswersJobService.class)
                    .setTag("thesis")
                    .setConstraints(Constraint.ON_ANY_NETWORK)
                    .setRecurring(false)
                    .setLifetime(Lifetime.FOREVER)
                    .setReplaceCurrent(true)
                    .setTrigger(Trigger.NOW)
                    .build();
            dispatcher.mustSchedule(job);
            // Mark the questionnaire as filled in
            SharedPreferences sharedPref = this.getSharedPreferences(
                    getString(R.string.shared_pref_file), Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putBoolean(getString(R.string.pref_filled_in), true);
            editor.apply();


            intent = new Intent(this, LogoutActivity.class);
        }
        else{
            intent = new Intent(this, QuestionActivityFactory.getActivity(question).getClass());
            intent.putExtra(QUESTION_NUMBER_KEY, question.sequenceNumber);
        }
        startActivity(intent);
    }



}
