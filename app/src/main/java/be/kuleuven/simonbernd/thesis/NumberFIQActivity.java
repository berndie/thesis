package be.kuleuven.simonbernd.thesis;

import android.arch.lifecycle.Observer;
import android.support.annotation.Nullable;
import android.view.ViewGroup;
import android.widget.NumberPicker;

import be.kuleuven.simonbernd.thesis.model.AnswerWrapper;
import be.kuleuven.simonbernd.thesis.model.Question;

/**
 * Created by Bernd on 18/03/2018.
 */

public class NumberFIQActivity extends FIQuestionActivity {
    private NumberPicker widget;

    @Override
    protected void setWidget(AnswerWrapper answerWrapper) {
        if(widget != null){
            widget.setValue(Integer.parseInt(answerWrapper.text));

        }
    }

    @Override
    protected void setQuestion(Question question) {
        super.setQuestion(question);
        createWidget();
        applyRules(question.type);
    }


    @Override
    protected void createWidget() {
        widget = new NumberPicker(this);
        ViewGroup layout = findViewById(R.id.root_layout);
        layout.addView(widget);
    }


    protected void applyRules(int questionNumber){
        switch(questionNumber){
            case Question.FRIENDS:
                widget.setMinValue(0);
                widget.setMaxValue(Integer.MAX_VALUE);
                break;
            case Question.AGE:
                widget.setMinValue(3);
                widget.setMaxValue(120);
                break;
        }
    }
}
