package be.kuleuven.simonbernd.thesis;

import android.content.Context;
import android.support.annotation.NonNull;

import be.kuleuven.simonbernd.thesis.model.Question;

/**
 * Created by Bernd on 17/03/2018.
 */

public class QuestionActivityFactory {

    public static QuestionActivity getActivity(@NonNull Question question){
        return getActivity(question.type);
    }

    public static QuestionActivity getActivity(int type) {
        switch(type){
            case Question.MC:
                return new MCQuestionActivity();
            case Question.MS:
                return new MSQuestionActivity();
            case Question.AGE:
            case Question.FRIENDS:
                return new NumberFIQActivity();
            case Question.TEXT:
                return new TextFIQActivity();
        }
        return null;
    }
}
