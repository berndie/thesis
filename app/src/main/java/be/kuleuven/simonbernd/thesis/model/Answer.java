package be.kuleuven.simonbernd.thesis.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Transaction;

/**
 * Created by Bernd on 15/03/2018.
 */
@Entity
public class Answer{
    @PrimaryKey(autoGenerate = true)
    public int id;
    public String text;

}
