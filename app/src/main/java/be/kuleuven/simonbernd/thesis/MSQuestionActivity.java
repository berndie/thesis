package be.kuleuven.simonbernd.thesis;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.GridLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;

import be.kuleuven.simonbernd.thesis.model.AnswerWrapper;

public class MSQuestionActivity extends QuestionActivity {


    private ViewGroup checkGroup;

    @Override
    protected void setAnswers(final List<AnswerWrapper> answers) {
        if(checkGroup == null){
            checkGroup = new GridLayout(this);
            ((ViewGroup) findViewById(R.id.root_layout)).addView(checkGroup);

            for(final ListIterator<AnswerWrapper> ansit = answers.listIterator(); ansit.hasNext();){
                CheckBox radioButton = new CheckBox(this);
                final AnswerWrapper answer = ansit.next();
                radioButton.setText(answer.text);
                radioButton.setChecked(answer.selected);
                radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        answer.selected = b;
                        model.selectAnswers(compoundButton.getContext(), questionNumber, answers);
                    }
                });
                checkGroup.addView(radioButton);
            }
        }
        else{
            for(int i = 0; i < answers.size(); ++i){
                ((CheckBox )checkGroup.getChildAt(i)).setChecked(answers.get(i).selected);
            }
        }

    }
}
