package be.kuleuven.simonbernd.thesis;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import be.kuleuven.simonbernd.thesis.model.Answer;
import be.kuleuven.simonbernd.thesis.model.AnswerWrapper;
import be.kuleuven.simonbernd.thesis.model.Question;
import be.kuleuven.simonbernd.thesis.model.QuestionAnswer;
import be.kuleuven.simonbernd.thesis.model.ThesisDatabase;

/**
 * Created by Bernd on 17/03/2018.
 */

public class QuestionViewModel extends ViewModel {
    public LiveData<List<AnswerWrapper>> answers;
    public LiveData<Question> question;
    private int currentQuestionNumber;

    public LiveData<List<AnswerWrapper>> getAnswers(Context context, int questionNumber) {
        if(answers == null){
            answers = ThesisDatabase.getAppDatabase(context).thesisDAO().getAnswers(questionNumber);
        }
        return answers;

    }

    public LiveData<Question> getQuestion(Context context, int questionNumber) {
        if(question==null || currentQuestionNumber != questionNumber){
            question = ThesisDatabase.getAppDatabase(context).thesisDAO().getQuestion(questionNumber);
            currentQuestionNumber = questionNumber;
        }
        return question;
    }

    public void selectAnswers(final Context context, final int questionNumber, final List<AnswerWrapper> selected){
        new Thread(new Runnable() {
            @Override
            public void run() {
                List<QuestionAnswer> qaList = new ArrayList<>();
                for(AnswerWrapper a : selected){
                    qaList.add(selectAnswerHelper(a, questionNumber));
                }
                ThesisDatabase.getAppDatabase(context).thesisDAO().selectAnswers(qaList);
            }
        }).start();

    }

    public void selectAnswer(final Context context, final int questionNumber, final AnswerWrapper selected){
        new Thread(new Runnable() {
            @Override
            public void run() {
                int x = ThesisDatabase.getAppDatabase(context).thesisDAO()
                        .selectAnswer(selectAnswerHelper(selected, questionNumber));
                Log.d("QuestionVM", "Rows inserted: " + x);
            }
        }).start();

    }

    private QuestionAnswer selectAnswerHelper(AnswerWrapper wrapper, int questionNumber){
        QuestionAnswer qa = new QuestionAnswer();
        qa.answerID = wrapper.id;
        qa.questionID = questionNumber;
        qa.selected = wrapper.selected;
        return qa;
    }


    public void newAnswer(final Context context, final int questionNumber, final AnswerWrapper selected){
        new Thread(new Runnable() {
            @Override
            public void run() {
                int x = ThesisDatabase.getAppDatabase(context).thesisDAO()
                        .selectAnswer(selectAnswerHelper(selected, questionNumber));
                Log.d("QuestionVM", "Rows inserted: " + x);
            }
        }).start();
    }

}
