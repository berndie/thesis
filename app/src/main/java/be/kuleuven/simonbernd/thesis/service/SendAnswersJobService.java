package be.kuleuven.simonbernd.thesis.service;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;
import javax.security.cert.X509Certificate;

import be.kuleuven.simonbernd.thesis.R;
import be.kuleuven.simonbernd.thesis.model.ThesisDAO;
import be.kuleuven.simonbernd.thesis.model.ThesisDatabase;

public class SendAnswersJobService extends JobService {
    public static final String URL = "https://berndie.ulyssis.be/thesis/test.cgi";

    static class DataToSend{
        public List<ThesisDAO.SendData> data;
        public String token;
    }
    @Override
    public boolean onStartJob(final JobParameters job) {
        new sendTask(job).execute(this);
        return true;
    }


    private static class sendTask extends AsyncTask<SendAnswersJobService,Void,Void>{

        private final JobParameters job;

        sendTask(JobParameters job){
            super();
            this.job = job;
        }
        @Override
        protected Void doInBackground(SendAnswersJobService... jss) {
            //TODO remove
            android.os.Debug.waitForDebugger();
            DataToSend d = new DataToSend();
            if(jss.length != 1){
                return null;
            }

            d.data = ThesisDatabase.getAppDatabase(jss[0]).thesisDAO().getAllSelected();
            d.token = FirebaseInstanceId.getInstance().getToken();
            Gson gson = new GsonBuilder().create();
            InputStream caInput = jss[0].getResources().openRawResource(R.raw.uly);
            RequestQueueSingleton.getQueue(jss[0], caInput)
                    .add(jss[0].postAnswers(gson.toJson(d), job));
            return null;
        }
    }
    @Override
    public boolean onStopJob(JobParameters job) {
        return true;
    }

    public JsonObjectRequest postAnswers (String data, final JobParameters job){
        try {
            Log.wtf("SendTaskService", data);
            return new JsonObjectRequest(Request.Method.POST, URL, new JSONObject(data),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("SendTaskService", "DONE");
                            jobFinished(job,false);
                        }
                    }
                    , new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    jobFinished(job,true);
                    Log.d("VolleyError", error.toString());
                }
            });
        } catch (JSONException e) {
            jobFinished(job,true);
            Log.d("JSONParseError", "Can't parse the answers to send");
            return null;
        }

    }


}
abstract class RequestQueueSingleton{
    private static final String HOSTNAME = "berndie.ulyssis.be";
    private static RequestQueue queue;

    static RequestQueue getQueue(Context context, InputStream caInput){
        if(queue == null){
            queue = Volley.newRequestQueue(context, new HurlStack(null, getSocketFactory(caInput)));
        }
        return queue;
    }
    private static SSLSocketFactory getSocketFactory(InputStream caInput){

            CertificateFactory cf = null;
            try {
                cf = CertificateFactory.getInstance("X.509");
                Certificate ca;
                try {
                    ca = cf.generateCertificate(caInput);
                    Log.e("CERT", "Certificate Generated!");
                } finally {
                    caInput.close();
                }

                // Make a new keystore and add the cert
                String keyStoreType = KeyStore.getDefaultType();
                KeyStore keyStore = KeyStore.getInstance(keyStoreType);
                keyStore.load(null, null);
                keyStore.setCertificateEntry("ca", ca);


                String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
                TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
                tmf.init(keyStore);

                // Add a verified hostname
                HostnameVerifier hostnameVerifier = new HostnameVerifier() {
                    @Override
                    public boolean verify(String hostname, SSLSession session) {

                        Log.e("CipherUsed", session.getCipherSuite());
                        return hostname.compareTo(HOSTNAME)==0;

                    }
                };

                HttpsURLConnection.setDefaultHostnameVerifier(hostnameVerifier);
                SSLContext context = SSLContext.getInstance("TLS");

                context.init(null, tmf.getTrustManagers(), null);
                HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());

                return context.getSocketFactory();

            } catch (CertificateException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (KeyStoreException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (KeyManagementException e) {
                e.printStackTrace();
            }

            return  null;
        }

}

