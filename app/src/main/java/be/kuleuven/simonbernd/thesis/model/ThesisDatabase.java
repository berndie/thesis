package be.kuleuven.simonbernd.thesis.model;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.support.annotation.NonNull;

import com.huma.room_for_asset.RoomAsset;

import be.kuleuven.simonbernd.thesis.R;


/**
 * Created by Bernd on 16/03/2018.
 */
@Database(entities = {Question.class, Answer.class, QuestionAnswer.class}, version = 2, exportSchema = false)
public abstract class ThesisDatabase extends RoomDatabase{
    private static final String DB_NAME = "questionnaire.db";
    private static ThesisDatabase INSTANCE;
    public abstract ThesisDAO thesisDAO();

    public static ThesisDatabase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE = RoomAsset.databaseBuilder(context, ThesisDatabase.class, DB_NAME)

            .build();

        }
        return INSTANCE;
    }


}
