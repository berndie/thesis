package be.kuleuven.simonbernd.thesis.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import java.util.List;


/**
 * Created by Bernd on 15/03/2018.
 */

@Entity(primaryKeys = {"questionID", "answerID"},
        foreignKeys = {
                @ForeignKey(entity = Question.class,
                        parentColumns = "sequenceNumber",
                        childColumns = "questionID"),
                @ForeignKey(entity = Answer.class,
                        parentColumns = "id",
                        childColumns = "answerID")
        })
public class QuestionAnswer {
    public int questionID;
    public int answerID;
    public boolean selected;

}
