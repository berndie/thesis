package be.kuleuven.simonbernd.thesis.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.Nullable;

/**
 * Created by Bernd on 14/03/2018.
 */

@Entity
public class Question{
    public static final int MC = 0;
    public static final int MS = 1;
    public static final int FRIENDS = 2;
    public static final int AGE = 3;
    public static final int TEXT = 4;


    @PrimaryKey(autoGenerate = true)
    public int sequenceNumber;

    public String text;
    public int type;

    public String columnName;
}
