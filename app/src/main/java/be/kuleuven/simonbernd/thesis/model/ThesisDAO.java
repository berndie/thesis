package be.kuleuven.simonbernd.thesis.model;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;
import android.arch.persistence.room.Update;
import android.support.annotation.Nullable;
import android.util.Pair;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Bernd on 15/03/2018.
 */
@Dao
public abstract class ThesisDAO {
    @Query("SELECT id, text, selected FROM QuestionAnswer " +
            "INNER JOIN Answer ON Answer.id = QuestionAnswer.answerID WHERE QuestionAnswer.questionID = :questionNumber;")
    public abstract LiveData<List<AnswerWrapper>> getAnswers(int questionNumber);

    @Query("SELECT * FROM Question WHERE sequenceNumber = " +
            "CASE WHEN :questionNumber < 1 THEN 1 " +
            "WHEN :questionNumber <= (SELECT MAX(sequenceNumber) FROM Question) THEN :questionNumber " +
            "ELSE -1 END;")
    public abstract LiveData<Question> getQuestion(int questionNumber);

    @Query("SELECT columnName, Answer.text FROM QuestionAnswer " +
            "INNER JOIN Answer ON QuestionAnswer.answerID = Answer.id " +
            "INNER JOIN Question ON QuestionAnswer.questionID = Question.sequenceNumber " +
            "WHERE selected = 1")
    public abstract List<SendData> getAllSelected();

    public class SendData{
        public String columnName;
        public String text;
    }

    @Update
    public abstract void selectAnswers(List<QuestionAnswer> select);

    @Update
    public abstract int selectAnswer(QuestionAnswer questionAnswer);


}


