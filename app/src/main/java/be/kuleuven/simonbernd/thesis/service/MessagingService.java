package be.kuleuven.simonbernd.thesis.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import be.kuleuven.simonbernd.thesis.R;

public class MessagingService extends FirebaseMessagingService {

    private static final String TAG = "MessagingService";

    @Override
    public void onMessageReceived(final RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        //TODO remove
        android.os.Debug.waitForDebugger();

        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
//            SharedPreferences.Editor editor = getApplicationContext()
//                    .getSharedPreferences(getString(R.string.shared_pref_file), Context.MODE_PRIVATE).edit();
//            editor.putBoolean(getString(R.string.pref_predicted_suicide), Boolean.parseBoolean(remoteMessage.getData().get("suicide")));
//            editor.apply();
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(),
                            remoteMessage.getNotification().getTitle(),
                            Toast.LENGTH_LONG).show();
                }
            });
        }
    }
}
