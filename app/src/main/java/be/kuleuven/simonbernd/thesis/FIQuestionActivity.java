package be.kuleuven.simonbernd.thesis;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.List;

import be.kuleuven.simonbernd.thesis.model.AnswerWrapper;
import be.kuleuven.simonbernd.thesis.model.Question;

public abstract class FIQuestionActivity extends QuestionActivity {

    @Override
    protected void setAnswers(List<AnswerWrapper> answers) {
        if(answers.size() > 0){
            setWidget(answers.get(0));
        }
    }


    protected abstract void setWidget(AnswerWrapper answerWrapper);

    protected abstract void createWidget();
}
