package be.kuleuven.simonbernd.thesis;

import android.arch.lifecycle.Observer;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import be.kuleuven.simonbernd.thesis.model.Answer;
import be.kuleuven.simonbernd.thesis.model.AnswerWrapper;

public class MCQuestionActivity extends QuestionActivity {

    private RadioGroup buttonGroup;

    @Override
    protected void setAnswers(final List<AnswerWrapper> answers) {
        ViewGroup layout = ((ViewGroup) findViewById(R.id.root_layout));
        if(buttonGroup == null){
            buttonGroup = new RadioGroup(this);
            layout.addView(buttonGroup);
            for(final ListIterator<AnswerWrapper> ansit = answers.listIterator(); ansit.hasNext();){
                RadioButton radioButton = new RadioButton(this);
                final AnswerWrapper answer = ansit.next();
                radioButton.setText(answer.text);
                radioButton.setChecked(answer.selected);
                radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        answer.selected = b;
                        ansit.set(answer);
                        model.selectAnswers(compoundButton.getContext(), questionNumber, answers);
                    }
                });
                buttonGroup.addView(radioButton);
            }

        }
        else{
            for(int i = 0; i < answers.size(); ++i){
                ((RadioButton )buttonGroup.getChildAt(i)).setChecked(answers.get(i).selected);
            }
        }



    }
}
