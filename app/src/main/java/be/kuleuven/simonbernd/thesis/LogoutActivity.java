package be.kuleuven.simonbernd.thesis;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LogoutActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logout);
        mAuth = FirebaseAuth.getInstance();

        FirebaseUser user = mAuth.getCurrentUser();
        if(user != null){
            Button button = findViewById(R.id.logout_button);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mAuth.signOut();
                    //https://stackoverflow.com/questions/39434797/how-to-properly-sign-out-of-facebook-on-android-with-firebase
                    LoginManager.getInstance().logOut();
                    Intent intent = new Intent(LogoutActivity.this, LoginActivity.class);
                    startActivity(intent);
                }
            });
            TextView textView = findViewById(R.id.userinfo);
            textView.setText(user.getDisplayName());
        }
        else{
            Log.w("LogoutActivity", "onCreate: " + "No valid user found!");
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }
    }
    @Override
    public void onBackPressed() {
    }
}
